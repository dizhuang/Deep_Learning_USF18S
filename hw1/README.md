# HW1 Solution                           
- Homework Difficulty Level: Very easy (sometimes known as "beginner", "novice", "basic", etc.)                      
- Programming Language: R (code and log are enclosed)                   
- How to deploy the code?                                  
--  Download and install R. https://www.r-project.org/                   
-- Download and install RStudio. https://www.rstudio.com/products/rstudio/download/                                  
-- Import the code and data.                      
-- Run it. O(∩_∩)O              

 - non-homogeneous linear regression
 -- After trying all 63 combinations, the best formula is: Y~X1+X2+X3+X4+X5+X6           
 -- The corresponding  best mean-square-error is 1463.754                 
 -- The best estimated coefficients are (the first column of numerical values are the estimated coefficients):      
-- (Intercept) = -9.970e+00     
-- X1 = 4.136e-01      
-- X2 = 2.922e-03       
-- X3 = -6.166e-03       
-- X4 = -1.851e-03       
-- X5 = 1.374e-05            
-- X6 = -8.361e-07 

 - homogeneous linear regression
 -- After trying all 63 combinations, the best formula is: Y~X1+X2+X3+X4+X5+X6             
 -- The corresponding  best mean-square-error is 1497.384                
 -- The best estimated coefficients are (the first column of numerical values are the estimated coefficients):   
-- X1 = 4.124e-01      
-- X2 = -1.888e-03       
-- X3 = -6.582e-03      
-- X4 = -1.650e-03       
-- X5 = 9.899e-06            
-- X6 = 4.834e-06                      
***           
Reach me at: zhuangdi1990@gmail.com               
End.       