# HW2 Solution                           
- Homework Difficulty Level: Very easy (sometimes known as "beginner", "novice", "basic", etc.)                      
- Programming Language: Java (code are enclosed)                   
- How to deploy the code?                                  
--  Eclipse + Java Maven Project    
--  One sample solution: 

| Test fold number        | MSE on test fold for exp #1           | MSE on test fold for exp #2  | MSE on test fold for exp #3  | MSE on test fold for exp #4  |
| ------------- |:-------------:| -----:|-----:|-----:|
| 1      | 0.0223 | 0.0162 | 0.0517 | 0.0231 |
| 2     | 0.0472      |   0.0465 |   0.0254 |   0.0041 |
| 3 | 0.0401      |    0.0474 |   0.0161 |   0.0027 |
| 4 | 0.0184      |    0.0250 |   0.0320 |   0.0322 |
| 5 | 0.0220      |    0.0378 |   0.0536 |   0.0577 |
| average of 25 times | 0.0216     |    0.0205 |   0.0205 |   0.0206 |

Where experiments 1 through 4 indicate the following specifications for your system:                 
- Exp #1 -> Learning rate of 0.01 and initial weights of 0.                
- Exp #2 -> Learning rate of 0.1 and initial weights of 0.            
- Exp #3 -> Learning rate of 0.01 and random initial weights with zero mean unity variance.                  
- Exp #4 -> Learning rate of 0.1 and random initial weights with zero mean unity variance.                    

***           
Reach me at: zhuangdi1990@gmail.com               
End.    
