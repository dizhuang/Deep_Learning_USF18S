package edu.usf.dizhuang.LogisticRegression;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Performs simple logistic regression + cross-validation (random shuffling) + early-stopping-rule.
 * Date: 03/07/2018
 * Time: 11:42 PM
 * 
 * @author Di Zhuang
 */

public class Logistic {
	
	// the learning rate
	private double rate;
	
	// the weights to learn
	private double[] weights;
	
	// the number of iterations 
	private int ITERATIONS = 10000;
	
	// error i-1
	private static double err_1=Double.MAX_VALUE;
	
	// error i
	private static double err_2=Double.MAX_VALUE;
	
	// early-stopping rule
	private static int Stop_N=10;
	
	// early-stopping count number
	private static int earlyStopRule_n=0;
	
	// the cross validation fold number
	private static int cvFold=5;
	
	// all unique labels
	private static List<Integer> unique_labels=new ArrayList<Integer>();
	
	// the data for cross validation training
	private static List<Instance> training_instances=new ArrayList<Instance>();
	
	// the data for cross validation early-stopping
	private static List<Instance> verification_instances=new ArrayList<Instance>();
	
	// the data for cross validation testing
	private static List<Instance> testing_instances=new ArrayList<Instance>();
	
	public Logistic(int n, boolean rdm) {
		this.rate = 0.0001;
		
		if(rdm){
			// all zeros
			weights = new double[n];
			for(int i=0;i<weights.length;i++)
				weights[i]=0.0;
		}
		else{
			// random numbers
			Random r = new Random();
			for(int i=0;i<weights.length;i++)
				weights[i]=r.nextGaussian();
		}
	}
	
	public Logistic(double rate, int n, boolean rdm) {
		this.rate = rate;
		
		if(rdm){
			// all zeros
			weights = new double[n];
			for(int i=0;i<weights.length;i++)
				weights[i]=0.0;
		}
		else{
			// random numbers
			Random r = new Random();
			weights = new double[n];
			for(int i=0;i<weights.length;i++)
				weights[i]=r.nextGaussian();
		}
	}
	
	private static double sigmoid(double z) {
		return 1.0 / (1.0 + Math.exp(-z));
	}
	
	public void train() {
		for (int n=0; n<ITERATIONS; n++) {
			for (int i=0; i<training_instances.size(); i++) {
				int[] x = training_instances.get(i).x;
				double predicted = classify(x);
				int label = training_instances.get(i).label;
				for (int j=0; j<weights.length; j++)
					weights[j] = weights[j] + rate * (label - predicted) * x[j];
			}
			System.out.println("iteration: " + n + " " + Arrays.toString(weights) + " mse: " + err_2);
			if(earlyStopRule_cv()){
				System.out.println("early stopped at iteration: " + n);
				break;
			}
		}
		System.out.println("weights: "+ Arrays.toString(weights));
		System.out.println("verification mse: " + err_2);
		System.out.println("testing mse: " + test_cv());
	}
	
	public double test_cv(){
		double[] trueLabels=new double[testing_instances.size()];
		double[] predictedLabels=new double[testing_instances.size()];
		
		for (int i=0;i<testing_instances.size();i++) {
			int[] x=testing_instances.get(i).x;
			trueLabels[i]=testing_instances.get(i).label;
			predictedLabels[i]=classify(x);
		}
		
		return MSE(trueLabels, predictedLabels);
	}
	
	public boolean earlyStopRule_cv(){
		double[] trueLabels=new double[verification_instances.size()];
		double[] predictedLabels=new double[verification_instances.size()];
		
		for (int i=0;i<verification_instances.size();i++) {
			int[] x=verification_instances.get(i).x;
			trueLabels[i]=verification_instances.get(i).label;
			predictedLabels[i]=classify(x);
		}
		
		err_2=MSE(trueLabels, predictedLabels);
		if(err_2>=err_1)
			earlyStopRule_n++;
		else
			earlyStopRule_n=0;
		err_1=err_2;
		
		if(earlyStopRule_n>=Stop_N)
			return true;
		else
			return false;
	}
	
	public double MSE(double[] a, double[] b){
		double mse=0.0;
		for(int i=0;i<a.length;i++)
			mse+=Math.pow((a[i]-b[i]), 2);
		return mse/(double)a.length;
	}

	private double classify(int[] x) {
		double logit = .0;
		for (int i=0; i<weights.length;i++) 
			logit += weights[i] * x[i];
		return sigmoid(logit);
	}

	public static class Instance {
		public int label;
		public int[] x;

		public Instance(int label, int[] x) {
			this.label = label;
			this.x = x;
		}
	}
	
	public static void readDataSet(String file) throws NumberFormatException, IOException {		
		int[] nLabels=new int[2];
		nLabels[0]=0;
		nLabels[1]=0;
		List<Instance> dataset = new ArrayList<Instance>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line="";
		// skip the first row
		line=br.readLine();
		while ((line=br.readLine()) != null) {
			String[] lines=line.split(",");
			int label=Integer.parseInt(lines[lines.length-1]);
			if(label==0)
				nLabels[0]++;
			else
				nLabels[1]++;
			
			int[] data=new int[lines.length-1];
			for(int i=0;i<lines.length-1;i++)
				data[i]=Integer.parseInt(lines[i]);
			Instance instance = new Instance(label, data);
			dataset.add(instance);
		}
		br.close();
		
		unique_labels.add(0);
		unique_labels.add(1);
//		int nTra=0;
//		int nVer=0;
//		int nTes=0;
		
		for(int i:unique_labels){
			int nTr=(int)(nLabels[i]*(cvFold-2)/cvFold);
			int nVe=(int)(nLabels[i]/cvFold);
//			nTra+=nTr;
//			nVer+=nVe;
//			nTes+=nLabels[i]-nTr-nVe;
			List<Instance> bags=new ArrayList<Instance>();
			for(Instance ins: dataset)
				if(ins.label==i)
					bags.add(ins);
			RandomPicks(bags, nTr, nVe);
		}
//		System.out.println(training_instances.size()+"\t"+verification_instances.size()+"\t"+testing_instances.size()+"\t"+nLabels[0]+"\t"+nLabels[1]);
	}
	
	public static void RandomPicks(List<Instance> bags, int nTr, int nVe){
		Random rand = new Random();
		int t=bags.size();
		for(int i=0;i<nTr;i++){
			int tmp=rand.nextInt(t);
			training_instances.add(bags.get(tmp));
			bags.remove(tmp);
			t--;
		}
		for(int i=0;i<nVe;i++){
			int tmp=rand.nextInt(t);
			verification_instances.add(bags.get(tmp));
			bags.remove(tmp);
			t--;
		}
		for(int i=0;i<bags.size();i++)
			testing_instances.add(bags.get(i));
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException{
		readDataSet("HW2_dataset.csv");
		Logistic logistic = new Logistic(0.01, training_instances.get(0).x.length, true);
		logistic.train();

		// real test
		/*int[] x1 = {1,1,0,0,0,1};
		System.out.println("prob(1|x) = " + logistic.classify(x1));
		int[] x2 = {-1,-1,-1,0,-1,0};
		System.out.println("prob(1|x2) = " + logistic.classify(x2));*/
	}
}
